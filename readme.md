## Como instalar TensorFlow en Windows


# Prerequisitos
Windows 7 o superior
Nvidia GPU (GTX 650 o superior)  
Anaconda con python 3.6 (or 3.5)  
CUDA Tool kit (version 8)  
CuDNN (version 6.0)  

# 1- Instalar CUDA Tool Kit
Bajar Version 8: https://developer.nvidia.com/cuda-80-ga2-download-archive  

La version 9 no es soportada por Tensorflow 1.4


# 2- Incluir variables de entorno

En inicio busca "Variables de entorno"  

Click en el boton de variables de entorno  
Click en la variable "path" y poner editar  
Añadir los siguientes paths    
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\bin  
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\libnvvp  
C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v8.0\extras\CUPTI\libx64  
  
  
# 3- Bajar CUDNN 6.0
ir a https://developer.nvidia.com/rdp/cudnn-download  
crearse una cuenta si es necesario  
bajar CUDNN 6.0 for CUDA tool kit 8.  
Extraer el archivo zip en algun lado, en este caso fue en C:\  
En variables de entorno añade un path a la carpeta bin. Por ejemplo  
C:\cuda\bin  
  
Si es que en uno de los pasos siguientes se encuentra con algun problema  
relacionado con path, reiniciar el equipo deberia solucionar
  
  
# 4- Actualizar los drivers de la GPU
Ir a http://www.nvidia.com/Download/index.aspx  
Seleccionar la version adecuada para la GPU e instalar  


# 5- Instalar Anaconda (python 3.6 o 3.5)

Ir a https://www.anaconda.com/download/  
Escoger python 3.6 (64bit) e instalar
importante es hacer una instalacion "Just for me" para evitar posibles problemas


# 6- Instalar Pycharm
Bajar desde https://www.jetbrains.com/pycharm/download/  
Crear una cuenta


# 7- Crear virtual enviroment en Pycharm
Ingresar el siguiente comando desde la terminal en Pycharm

```
conda create -n tensorflow python=3.5
```
debe ser con python 3.5  


# 8- Instalar Tensorflow
Habiendo seleccionado el entorno virutal que se creo ingresar el comando  
```
  pip install --ignore-installed --upgrade tensorflow-gpu  
```

Instrucciones adicionales para instalarlo en modo solo CPU  
https://www.tensorflow.org/install/install_windows


# 9- Establecer entorno virtual desde pycharm  
Para establecerlo desde pycharm  
File -> settings -> proyect interpreter -> Addlocal -> Existing enviroment  
Y seleccionar el entorno virtual que se creo en el punto 8

![Alt text](pycharm-env.png?raw=true "Optional Title")


# 10- Verificar la instalacion
en el terminal escribir  
```
  python  
```
cuando el interprete este cargado  

```python
	>>> import tensorflow as tf  
	>>> hello = tf.constant('Hello, TensorFlow!')  
	>>> sess = tf.Session()    
	>>> print(sess.run(hello))  
```
si la instalacion estuvo correcta la salida deberia ser  
  
```python
hello, TensorFlow!  
```


# 11- Instalar Keras
simplemente ingresar el comando  
```
pip install keras  
```


# 12- Comprobar funcionamiento

para comprobar el funcionamiento se puede correr alguno de los ejemplos en  
https://github.com/keras-team/keras/tree/master/examples


# 13- Librerias adicionales

Probablemente seran requeridas librerias adicionales como h5py, para instalarlas  
usar pip, conda o el package manager de pycharm, si no detecta la libreria que  
instalaste reiniciar Pycharm, si eso no ayuda, intentar otro metodo de  
instalacion de ellas



